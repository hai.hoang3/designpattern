<?php

namespace CommandPattern;
include("Invoker.php");

class CommandTest 
{
    public function testCommand()
    {
        # code...
        $invoker = new Invoker();
        $invoker->run();
    }
}