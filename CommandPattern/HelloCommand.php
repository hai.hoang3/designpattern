<?php

namespace CommandPattern;

/**
 * This concrete command calls "print" on the Receiver, but an external
 * invoker just knows that it can call "execute"
 */
class HelloCommand implements Command
{
    
    /**
     * execute and output "Hello World".
     */
    public function execute()
    {
        echo "hello";
    }
}
