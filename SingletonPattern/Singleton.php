<?php

namespace SingletonPattern;
final class Singleton
{
    private $name;
    private static ?Singleton $instance = null;

    private function Singleton($name)
    {
        $this->name = $name;
    }

    // return name table in db
    public function getName()
    {
        return $this->name;
    }


    public static function getInstance(): Singleton
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }
}