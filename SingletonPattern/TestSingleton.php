<?php

namespace SingletonPattern;
include('Singleton.php');

    $product = Singleton::getInstance("Product");
    echo $product->getName();