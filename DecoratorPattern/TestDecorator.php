<?php

namespace DecoratorPattern;
include('CarDecorator.php');

// 1. Tạo một xe cơ bản
$basicCar = new Suv();

// 2. Thêm tùy chọn cửa sổ trời
$carWithSunRoof = new SunRoof($basicCar);


// 3. Thêm tùy chọn GPS
$carFullOption = new GPSNavigation($carWithSunRoof);

// 4. Kiểm tra xe với tùy chọn đầy đủ
echo $carFullOption-> description();
echo " giá " . $carFullOption-> cost() . "$";