<?php

namespace DecoratorPattern;

interface Car 
{
    public function cost();

    public function description();
}

class Suv implements Car {
    function cost() {
      return 30000;
    }

    function description () {
      return "Xe SUV";
    }
}

abstract class CarFeature implements Car {
    protected $car;
    function __construct(Car $car) {
      $this->car = $car;
    }
  
    abstract function cost();
    abstract function description();
  }

class SunRoof extends CarFeature {
    function cost () {
        return $this->car->cost() + 3000;
    }

    function description() {
        return $this->car->description() . " thêm cửa sổ trời";
    }
}

class GPSNavigation extends CarFeature {
    function cost () {
        return $this->car->cost() + 1800;
    }

    function description() {
        return $this->car->description() . " thêm GPS";
    }
}