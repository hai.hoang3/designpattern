DesignPatterns
Creationals Design Pattern
•	Singleton Pattern: là design pattern được sử dụng để đảm bảo mỗi 1 class chỉ đảm nhận 1 instance -> cải thiện performance . Tránh được việc khởi tạo quá nhiều object
•	Factory Pattern: là design pattern cho phép tạo nhiều đối tượng khác nhau từ nhiều class -> mở rộng project
Structural Design Pattern
•	Decorator Pattern: là design pattern cho phép them 1 chức năng mới vào đối tượng đã có mà không làm ảnh hưởng tới các đối tượng khác. Nó giúp giải quyết các vấn đề liên quan tới đa kế thừa bằng hệ thống composition 
•	Facade Pattern: Là design pattern biến việc sử dụng nhiều class với các method khác nhau về chung 1 interface đơn giản với 1 method
Behavioral Design Pattern
•	Strategy Pattern: Là design pattern cho phép thay đổi các cách thức thực thi đối tượng trong các thời điểm khác nhau.
•	Command Pattern: Là design pattern giúp đóng gói tất cả các thông tin cần thiết vào một đối tượng để thực thi hay thực hiện 1 event sau đó. 
