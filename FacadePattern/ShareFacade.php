<?php 

namespace FacadePattern;
include('Facade.php');

class ShareFacade {
    protected $facebook;    
    protected $googleplus;   
    protected $twitter;    
  
    // Các đối tượng được truyền vào phương thức khởi tạo  
    function __construct(Facebook $facebookObj,Google $googleObj,Twitter $twitterObj) {
      $this->facebook= $facebookObj;
      $this->googleplus= $googleObj;
      $this->twitter= $twitterObj;
    }  
  
    // Phương thức này thực hiện tất cả các yêu cầu chia sẻ lên mạng xã hội
    function share($url,$title,$status) {
      $this->facebook->share($status, $url);
      $this->googleplus->share($url);
      $this->twitter->up($url, $title);
    }
  }