<?php

namespace FacadePattern;
include('ShareFacade.php');

// Tạo đối tượng
$facebookObj = new Facebook();
$googleplusObj   = new Google();
$twitterObj  = new Twitter();

// Truyền các đối tượng này cho ShareFacade
$shareObj = new ShareFacade($facebookObj, $googleplusObj, $twitterObj);

// Gọi một phương thức để chia sẻ tất cả lên mạng xã hội
$shareObj->share('URL', 'title', 'content');

echo " Success!!";