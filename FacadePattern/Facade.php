<?php 

namespace FacadePattern;

// Class share gì đó lên Facebook
class Facebook {
    function share($status, $url) {
      echo (' Facebook:' . $status . ' from:' . $url);
    }
  }
  
  // Class share gì đó lên Google+.
  class Google {
    function share($url) {
      echo (' Shared on Google+:' . $url);
    }
  }
  
  // Class up lên Twitter
  class Twitter {
    function up($url, $title) {
      echo (' Twitter url:' . $url . ' title:' . $title);
    }
  }